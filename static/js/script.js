$(document).ready(function() {
	cariData('Harry Potter');
})

function masukinData (){
		var key = $('#form').val();
		('#form').val("")
		cariData(key);
}

function cariData (key){
	$('#hasil').empty();
	$.ajax({
		method: 'GET',
		url: '/ambilData/' + key,
		success: function(response) {
			console.log(response);
			for(let i=0; i<response.items.length; i++){
				$('#hasil').append('<tr>');

				var cover = response.items[i].volumeInfo.imageLinks;
				if (typeof cover != 'undefined') {
					cover = cover.smallThumbnail;
					$('#hasil').append('<td><img src="' + cover + '"></td>');
				}
				else {
					$('#hasil').append('<td>No image available</td>');
				}

				var title = response.items[i].volumeInfo.title;
				$('#hasil').append('<td>' + title + '</td>');

				var author = response.items[i].volumeInfo.authors;
				$('#hasil').append('<td>' + author + '</td>');

				var date = response.items[i].volumeInfo.publishedDate;
				$('#hasil').append('<td>' + date + '</td>');

				$('#hasil').append('</tr>');
			}
		}
	})
}