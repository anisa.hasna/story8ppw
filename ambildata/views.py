from django.shortcuts import render
from django.http import JsonResponse
import requests

# Create your views here.

def index(request):
	return render(request,'ambildata.html')

def getData(request, key):
	# key = request.GET['key']
	url = 'https://www.googleapis.com/books/v1/volumes?q=' + key

	response = requests.get(url)
	response_json = response.json()

	return JsonResponse(response_json)
