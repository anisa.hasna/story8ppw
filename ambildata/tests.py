from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
# from .models import statusModel
from .views import index, getData

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time
from datetime import datetime

# Create your tests here.
class unitTest(TestCase):
	def test_apakah_ada_ambil_data_page(self):
		c = Client()
		response = c.get("/")
		self.assertEqual(response.status_code, 200)

	def test_apakah_pake_template_ambildatahtml(self):
		response = Client().get('/')
		self.assertTemplateUsed(response, 'ambildata.html')

	def test_apakah_pake_fungsi_index(self):
		found = resolve("/")
		self.assertEqual(found.func, index)

	def test_apakah_ada_tulisan_cari_buku(self):
		c = Client()
		response = c.get("/")
		content = response.content.decode('utf8')
		self.assertIn("Yuk Cari Buku Favoritmu!", content)

	def test_apakah_ada_form(self):
		c = Client()
		response = c.get("/")
		content = response.content.decode('utf8')
		self.assertIn("<input", content)

	def test_apakah_ada_button_search(self):
		c = Client()
		response = c.get("/")
		content = response.content.decode('utf8')
		self.assertIn("<input", content)
		self.assertIn("Search", content)

class functionalTest(LiveServerTestCase):
	def setUp(self):
		chrome_options = Options()
		chrome_options.add_argument('--dns-prefetch-disable')
		chrome_options.add_argument('--no-sandbox')        
		chrome_options.add_argument('--headless')
		chrome_options.add_argument('disable-gpu')
		self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
		super(functionalTest, self).setUp()

	def tearDown(self):
		self.selenium.quit()
		super(functionalTest, self).tearDown()

	def test_apakah_web_terbuka_dan_input_dapat_dimasukkan_serta_ditampilkan(self):
		selenium = self.selenium
		selenium.get(self.live_server_url)
		tmp_input = selenium.find_element_by_id('form')
		tmp_submit = selenium.find_element_by_id('search')

		tmp_input.send_keys('Harry Potter')
		tmp_submit.send_keys(Keys.RETURN)